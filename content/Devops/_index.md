+++

title = "Devops Enable Specalism"

outputs = ["Reveal"]

[reveal_hugo]
theme = "white"

[logo]
src = "../qa.png"
width = "46px"


+++

# Instructor

Instructor: Leon L. Robinson

<https://linkedin.com/in/leonlrobinson>

- RHCA, RHCE
- Linux+, Network+, Security+
- TAP Diploma in Learning Facilitation
- Fantastically handsome

---

# Content Outline

- Flask
- Linux
- CI/CD
- Containerisation
- Configuration
- Infrastructure as Code
- Enterprise Container Deployment

---
{{% section %}}
{{< slide id="Flask" background="#004050" >}}

# Flask

---

{{< slide id="Flask" background="#004050" >}}

- Introduction
- Routes
- Database
- Templating and jinja2
- User input introduction

---

{{< slide id="Flask" background="#004050" >}}

- Creating forms
- Validators in forms
- Flask unit testing
- Flask integration testing
- Gunicorn
- Secrets-hashing

{{% /section %}}

---

{{% section %}}

# Flask Introduction

---

## Objective

By the end of the session you will be able to create a basic web application using the Flask Python Framework

---

- Frameworks
- Flask
- Structure
- Installation

{{% /section %}}

---

{{% section %}}

# Routes

---

## Objective

By the end of the session you will be able to access your application on different urls and pass data.

---

- Creating routes
- HTTP methods for managing data
- Dynamic URLS
- Redirects

{{% /section %}}

---

{{% section %}}

# Database

---

- Overview
- Configuration
- Schema
- Relationships
- CRUD

---

# Configuration

## Objective

By the end of the session you will be able to configure your application to use a database

---

- In-built vs Remote
- Creation
- Connector
- Environment Variables
- Creating Database objects

---

# Schema

## Objective

By the end of the session you will be able to use python to describe and create the database objects.

---

- Models
- Columns
- Types
- Constraints

---

# Relationships

## Objective

By the end of the session you will be able to use python to define relationships between data.

---

- One to Many
- Attributes
- Many to Many

---

# CRUD

## Objective

By the end of the session you will be able to Create, Read, Update and Delete data.

---

- Create
- Read
  - All, First, Filter, Get, Order, Limit, Count.
- Update
- Delete

{{% /section %}}

---

{{% section %}}

# Templating and jinja2

---

## Objective

By the end of the session you will be able to use templates to display data.

---

- Jinja2
- Expressions
- Statements
- Blocks
- Extending Files
- Selection
- Iteration
- Rendering
- Structure

{{% /section %}}

---

{{% section %}}

# User input introduction

---

## Objective

By the end of the session you will be able to offer web based user interaction in your application.

---

- Forms
- CSRF

---

# Creating

---

- Creating a form
- Field types
- Populating Selectfield

---

# Validators in forms

---

- Built In Validators
- Data Required
- Email
- EqualTo
- Length
- Custom validators
- Validating

{{% /section %}}

---

{{% section %}}

# Flask unit testing

---

## Objective

By the end of the session you will be able to write unit tests for your Flask application.

---

- The TestBase class
- create_app()
- setUp()
- tearDown()
- HTTP requests
- Assertations
- Coverage

{{% /section %}}

---

{{% section %}}

# Flask integration testing

---

## Objective

By the end of the session you will be able to simulate user interaction with your web application.

---

- Setup
- Test Cases
- XPaths
- Selenium

{{% /section %}}

---

{{% section %}}

# Gunicorn

---

## Objective

By the end of the session you will be able to deploy your application as a robust service.

---

- WSGI
- Installation
- Usage

{{% /section %}}

---

{{% section %}}

# Secrets-hashing

---

## Objective

By the end of the session you will be able to store sensitive information security within your application

---

- What is Hashing
- Bcrypt

{{% /section %}}

---

{{% section %}}
{{< slide id="Linux" background="#004050" >}}

# Linux

- Introduction
- Linux distributions
- Bash interpreter
- Sessions
- Files and directories

---
{{< slide id="Linux" background="#004050" >}}

- Editing text
- Aliases functions-variables
- User administration
- Ownership
- Data streams
- Pipes and filters

---
{{< slide id="Linux" background="#004050" >}}

- Scripting
- Sudoers
- Managing-systemd-services
- Systemd-service-configuration
- Openssh

{{% /section %}}

---

{{% section %}}

# Introduction

---

## Objective

By the end of the session you will understand the history, uses and ecosystem.

---

- Overview
- Anatomy of the OS
- Linux
- History of Linux
- Linux & Windows
- Uses of Linux
- Linux Distributions
- Linux Software Packages

{{% /section %}}

---

{{% section %}}

# Linux distributions

---

## Objective

By the end of the session you will understand the different focus and purposes of Linux Distributions

---

- What is a Linux Distribution?
- Linux Packages
  - Package Managers
- Red Hat and Debian

{{% /section %}}

---

{{% section %}}

# Bash interpreter

---

## Objective

By the end of the session you will be able to interact with a Linux OS using the Bash shell.

---

- Overview
- The Shell
- Reading Commands
- Shell Metacharacters
- Globbing
- Ignoring Metacharacters
- Variables and Command Substitution

{{% /section %}}

---

{{% section %}}

# Sessions

---

## Objective

By the end of the session you will be able to manage login session on Linux with simple commands.

---

- Logging In
  - Credentials
  - User ID
- The Command Line
  - Shell
  - How the command line looks
  - Common Commands
  - Working with text files
- Getting help in Linux
  - CLI help
  - Online help

{{% /section %}}

---

{{% section %}}

# Files and directories

---

## Objective

By the end of the session you will be able to understand the filesystem hierachy and manage files and their location.

---

- Managing Files and Directories
- Relative or Absolute Paths
- Wildcards (Globbing)
- Variables in Linux
- Making a File
- Copying Files
- Moving Files
- Removing Files
- Directories

{{% /section %}}

---

{{% section %}}

# Editing text

---

## Objective

By the end of the session you will be able to edit text using Command Line Editors

---

- Linux Text Editors
- Command Mode
- Screen Navigation
- Inserting
- Deleting Commands
- Changing Text
- Cut, Copy and Paste
- Alternative Editors

{{% /section %}}

---

{{% section %}}

# Aliases functions-variables

---

## Objective

By the end of the session you will be able to manage variables and aliases persistently.

---

- Shell variables
- Setting variables
- Some standard Shell Variables
  - The Path Variable
- Aliases
- Setting Aliases
- Functions
- Defining functions
- Persistence

{{% /section %}}

---

{{% section %}}

# User administration

---

## Objective

By the end of the session you will be able to manage users and administrative privileges.

---

- Users and Groups
  - Authentication vs Authorisation
  - Adding a user
  - Adding a group
  - Adding users to groups
- The Super User
  - Working as the Super User
- Switching User
  - The `su` command
  - Sudo
    - Sudo Sessions
    - Sudoers

{{% /section %}}

---

{{% section %}}

# Ownership

---

## Objective

By the end of the session you will be able to understand and manage linux file ownership and permissions

---

- Access Attributes
- File Ownership
- File Permissions
  - The Super User
- File Protection
- What permissions are needed?
- Setting Permissions
- Setting file ownership

{{% /section %}}

---

{{% section %}}

# Data streams

---

## Objective

By the end of the session you will be able to manage data from and between CLI applications.

---

- Stream Types
  - Standard Output (`stdout`)
  - Standard Input (`stdin`)
  - Standard Error (`stderr`)
- Redirection
  - Redirect To A File (`>`)
    - Appending (`>>`)
  - Standard Input (`<`)
  - Heredocs (`<<`)
  - Piping
- Redirection with File Descriptors
  - Standard Error Redirection
    - To a File
    - To Standard Output

{{% /section %}}

---

{{% section %}}

# Pipes and filters

---

## Objective

By the end of the session you will be able to use Command Line tools to manipulate and filter text data.

---

- Pipes
  - A Quick Recap
  - About Pipes
- Filters
  - tee
  - grep
  - sort
  - uniq
  - tr

{{% /section %}}

---

{{% section %}}

# Scripting

---

## Objective

By the end of the session you will be able to perform simple automation with bash scripting

---

- Creating a Script
- Selection
- Variables
- Input
- Iteration

{{% /section %}}

---

{{% section %}}

# Sudoers

---

## Objective

By the end of the session you wil be able to manage administrative access for user accounts.

---

- Configuring a sudo User
  - Run sudo Commands Without a Password
  - Only Allow Specific Commands

{{% /section %}}

---

{{% section %}}

# Managing-systemd-services

---

## Objective

By the end of the session you will be able to manage services on a Linux OS.

---

- systemctl
  - Listing Services
  - Starting a Service
  - Stopping a Service
  - Restarting a Service
  - Reloading a Service Configuration
  - Enabling a Service
    - Enable and Start
  - Disable a Service
  - Check the Service Status

{{% /section %}}

---

{{% section %}}

# Systemd-service-configuration

---

## Objective

By the end of the session you will be able to manage a custom service on a Linux OS.

---

- Configuration
  - ExecStart
  - User
  - WorkingDirectory
  - Environment
- Installation

{{% /section %}}

---

{{% section %}}

# Openssh

---

## Objective

By the end of the session you will be able to remotely manage a Linux OS using OpenSSH

---

- SSH Clients
- Installation
- Key Pairs
- Authorized Keys
- Known Hosts
  - Entry Data
  - Managing the Known Hosts
    - Using Default Behaviour
    - Using `ssh-keyscan` to Add Hosts
    - Removing Hosts
- SSH Commands
  - Terminal Session
  - Connect As Another User
  - Running a Single Command
    - Multiple Commands
  - Specify Private Key File

{{% /section %}}

---

{{% section %}}
{{< slide id="Jenkins" background="#004050" >}}

# Jenkins introduction

---

## Objective

By the end of the session you will be able to deploy Jenkins and create automated application builds.

---

- Installation wizard
- Jobs
- Freestyle project
- Builds
- Plugins

{{% /section %}}

---

{{% section %}}

{{< slide id="Docker" background="#004050" >}}

# Docker

- Introduction
- Images
- Containers
- Dockerfiles
- Dockerfile instructions

---
{{< slide id="Docker" background="#004050" >}}

- Dockerignore
- Multi stage build
- Networking
- Registry
- Bind mounts
- Volumes

{{% /section %}}

---

{{% section %}}

# Introduction

---

## Objective

By the end of the session you will be able to explain the advantages of containerised application deployment.

---

- Containers
  - Containers vs Virtual Machines
  - Virtual Machines
  - Containers
- Installation
  - Linux
  - Windows and MacOS

{{% /section %}}

---

{{% section %}}

# Images

---

## Objective

By the end of the session you will be able to explain image structure and manage images using the CLI.

---

- Images
  - Image layers
  - Registry
  - Image properties
- Docker image commands

{{% /section %}}

---

{{% section %}}

# Containers

---

## Objective

By the end of the session you will be able to explain the use case for containers and manage them using the CLI.

---

- Use Case
- Containers
- Starting up a container
- Executing commands in container
- Start, stop, rename and remove existing containers
- docker rename

---

![Docker Commands](/DockerCommandsSlide.png "Docker Commands")

{{% /section %}}

---

{{% section %}}

# Dockerfiles

---

## Objective

By the end of the session you will be able to automate building of Docker images.

---

- Dockerfiles
  - Context
  - Instructions
  - docker build

---

# Dockerfile instructions

---

- Instructions

{{% /section %}}

---

{{% section %}}

# Dockerignore

---

## Objective

By the end of the session you will be able to exclude files from your project directory from being built into container images.

---

- The .dockerignore file
- Usage

{{% /section %}}

---

{{% section %}}

# Multi stage build

---

## Objective

By the end of the session you will be able to automate a multi-stage container image build.

---

- Source Code and Build Tools
- Multi-stage Builds
- Dockerfile Syntax

{{% /section %}}

---

{{% section %}}

# Networking

---

## Objective

By the end of the session you will be able to explain and manage different container networking scenarios.

---

- Networking
- Network Types
  - Bridge Networks
  - Overlay Networks
  - Host Networks
  - Macvlan Networks
- Bridge Networks
  - Default
  - User-Defined
- Managing networks

{{% /section %}}

---

{{% section %}}

# Registry

---

## Objective

By the end of the session you will be able to manage a registry and images in registries.

---

- Storing and Pulling Images
  - Naming Images for Registries
- Creating Your Own Registry
  - Benefits
- Accessing a Registry Remotely

{{% /section %}}

---

{{% section %}}

# Bind mounts

---

## Objective

By the end of the session you will be able to manage persistent data for container applications.

---

- Bind mounts
  - Use case
  - Command

{{% /section %}}

---

{{% section %}}

# Volumes

---

## Objective

By the end of the session you will be able to manage persistant container data using Docker Volumes.

---

- Volumes
  - Benefits
- Managing Volumes
- Mounting Volumes
- Volume Drivers
- Tutorial

{{% /section %}}

---

{{% section %}}
{{< slide id="DockerCompose" background="#004050" >}}

# Docker Compose

---

## Objective

By the end of the session you will be able to manage multiple container application deployments.

---

- Compose introduction
- Compose cli
- Compose configuration

{{% /section %}}

---

{{< slide id="DockerSwarm" background="#004050" >}}
# Docker swarm

- Swarm introduction
- Swarm cli
- Swarm ingress
- Swarm stack
- Orchestration tool comparison

 ---

{{% section %}}

# Swarm introduction

---

## Objective

By the end of the session you will be able to explain the benefits of container orchestration and deploy a simple cluster.

---

- Container Orchestration
  - Benefits
- Docker Swarm
  - Swarm Clusters
  - Manager Nodes
  - Worker Nodes
  - Ingress

{{% /section %}}

---

{{% section %}}

# Swarm cli

---

## Objective

By the end of the session you will be able to deploy, update, access and remove container applications in a cluster.

---

- Node Management
- Services
  - Create
  - Publishing Ports and Updating Services

{{% /section %}}

---

{{% section %}}

# Swarm ingress

---

## Objective

By the end of the session you will be able to load balance network traffic accross a cluster.

---

- Network Mesh
- Load balancing Across Nodes
  - Benefits of our external load balancer

{{% /section %}}

---

{{% section %}}

# Swarm stack

---

## Objective

By the end of the session you will be able to deploy multi-container applications in a cluster

---

- Managing and Updating Services
- Stacks
  - Deploying and Updating
  - Commands
  - Configuration File
  - Pulling Images

{{% /section %}}

---

{{% section %}}

# Orchestration tool comparison

{{% /section %}}

---

{{% section %}}
{{< slide id="Pyvanced" background="#004050" >}}

# Python advanced

---

## Objective

By the end of the session you will be able to write a python client application to interact with a http api.

---

- Http requests
- Api
- Unittest mock

{{% /section %}}

---
{{% section %}}
{{< slide id="Nexus" background="#004050" >}}

# Nexus

---

## Objective

By the end of the session you will be able to host and cache images in a local repository. 

---

- Introduction
- Docker private repo image cache

{{% /section %}}

---

{{% section %}}
{{< slide id="Nginx" background="#004050" >}}

# Nginx

---

## Objective

---

By the end of the session you will be able to manage Nginx as a simple proxy and server

---

- Introduction
- Configuration
- Http reverse proxy configuration
- Web server configuration
- Load balancing

{{% /section %}}

---

{{% section %}}
{{< slide id="JenkinsPipe" background="#004050" >}}

# Jenkins pipeline

---

## Objective

By the end of the session you will be able to create more advanced automation procedures in Jenkins

---

- Agents
- Pipeline
- Pipeline snippet generator
- Secrets-management

{{% /section %}}

---

{{% section %}}
{{< slide id="JenkinsAdv" background="#004050" >}}

# Jenkins advanced

---

## Objective

By the end of the session you will be able to integrate Jenkins with other tools.

---

- Integrating with other tools
- Scm
- Python flask freestyle project
- Freestyle selenium project
- Init

{{% /section %}}

---

{{% section %}}
{{< slide id="Ansible" background="#004050" >}}

# Ansible

- Introduction
- Playbooks
- Inventory
- Facts and variables
- Roles

{{% /section %}}

---

{{% section %}}

# Introduction

---

## Objective

By the end of the session you will be able to explain ansibles architecture and deploy it.

---

- Architecture
- Installation

{{% /section %}}

---

{{% section %}}

# Playbooks

---

## Objective

By the end of the session you will be able to write a playbook to configure remote systems.

---

- Configuration
  - Hosts
  - Tasks & Modules

{{% /section %}}

---

{{% section %}}

# Inventory

---

## Objective

By the end of the session you will be able to use an inventory to customise ansible automated configuration.

---

- Configuration Overview
  - Inventory Files for Different Environments
- Inventory Parameters
  - Applying to a Host
  - Applying to a Group of Hosts
  - Ansible User
  - SSH Private Key File
  - SSH Arguments

{{% /section %}}

---

{{% section %}}

# Facts and vars

---

## Objective

By the end of the session you will be able to use variables and facts to further customise anible configuration.

---

- Variables
- Scope
- Defining variables in a playbook
- Defining variable on the CLI
- Setting a default variable value

{{% /section %}}

---

{{% section %}}

# Roles

---

## Objective

By the end of the session you will be able to apply roles to improve code efficiency

---

- Using Roles
  - Playbook Configuration
    - Variables
  - Directory Structure

{{% /section %}}

---
{{< slide id="Terraform" background="#004050" >}}

# Terraform

- Introduction
- Basic syntax
- Data types
- Variables
- Modules
- Intermediate syntax
- Tf branch model

---
{{% section %}}

# Introduction

---

## Objective

By the end of the session you will be able to explain how Terraform is used to manage Infrastructure as Code

---

- Infrastructure as Code
- Workflows
- Use Cases

{{% /section %}}

---
{{% section %}}

# Basic syntax

---

## Objective

By the end of the session you will be able to write configuration files for Terraform.

---

- Blocks
- Providers
- Resources
- Variables
- Outputs
- Comments
- Ordering
- Formatting

{{% /section %}}

---
{{% section %}}

# Data types

---

## Objective

By the end of the session you will be able to use Data Types in Terraform Configurations.

---

- Strings
- Numbers
- Booleans
- Lists
- Maps

{{% /section %}}

---
{{% section %}}

# Variables

---

## Objective

By the end of the session you will be able to use variables in Terraform configuration

---

- Defaults
- Providing variables
- Precedence

{{% /section %}}

---

{{% section %}}

# Modules

---

## Objective

By the end of the session you will be able to collect configurations files into modules

---

- Structure

{{% /section %}}

---

{{% section %}}

# Intermediate syntax

---

## Objective

By the end of the session you will be able to use intermediate programatic syntax

---

- Meta Arguments
  - depends_on
  - count
  - for_each
  - provider
  - lifecycle
    - create_before_destroy
    - prevent_destroy
    - ignore_changes
- Operation timeouts

{{% /section %}}

---

{{% section %}}

# Branch model

---

## Objective

By the end of the session you will understand how to use the branch model to support multiple environments.

---

- Overview

{{% /section %}}

---

{{< slide id="Kube" background="#004050" >}}
{{% section %}}

# Kubernetes

- Introduction
- Elastic kubernetes service
- Azure kubernetes service
- Google kubernetes engine
- Kubectl

---
{{< slide id="Kube" background="#004050" >}}

- Pods
- Services
- Controllers
- Labels and selectors
- Namespaces
- Configmaps

---
{{< slide id="Kube" background="#004050" >}}

- Secrets
- Sidecar model
- Logging
- Managing container resources
- Network policies
- Security contexts

{{% /section %}}

---

{{% section %}}

# Introduction

---

## Objective

By the end of the session you will be able to describe Kubernetes architecture

---

- Structure
- Pods
- Services
- Controllers
- Nodes
- Control Plane

{{% /section %}}

---

{{% section %}}

# Elastic kubernetes service

---

## Objective

By the end of the session you will be able to deploy a Kubernetes cluster in AWS

{{% /section %}}

---

{{% section %}}

# Azure kubernetes service

---

## Objective

By the end of the session you will be able to deploy a Kubernetes cluster in Azure

{{% /section %}}

---

{{% section %}}

# Google kubernetes engine

---

## Objective

By the end of the session you will be able to deploy a Kubernetes cluster in GCP

{{% /section %}}

---

{{% section %}}

# Kubectl

---

## Objective

By the end of the session you will be able to manage Kubernetes with a command line tool.

---

- kubectl
  - Installation
  - Configuration
- Commands
- Manifests

{{% /section %}}

---

{{% section %}}

# Pods

---

## Objective

By the end of the session you will be able to describe and manage Kubernetes pods.

---

- Resource Templates
- Pod Lifecycle
- Pod Commands

{{% /section %}}

---

{{% section %}}

# Services

---

## Objective

By the end of the session you will be abel to manage container application networking using Services

---

- Services
- Types
  - ClusterIP
  - Node Port
  - Load Balancer

{{% /section %}}

---

{{% section %}}

# Controllers

---

## Objective

By the end of the session you will be able to manage system states with Controllers.

---

- Control Loop
- Controller Types
  - Replica Sets
  - Deployments

{{% /section %}}

---

{{% section %}}

# Labels and selectors

---

## Objective

By the end of the session you will be able to organise Kubernetes resources with labels.

---

- Labels
- Filtering

{{% /section %}}

---

{{% section %}}

# Namespaces

---

## Objective

By the end of the session you will be able to manage resources into virtual clusters using namespaces.

---

- Managing Namespaces
- Using Namespaces

{{% /section %}}

---

{{% section %}}

# Configmaps

---

## Objective

By the end of the session you will be able to manage container application configuration with ConfigMaps.

---

- Use Cases
- Manifest

{{% /section %}}

---

{{% section %}}

# Secrets

---

## Objective

By the end of the session you will be able to manage sensitive data in container applications with secrets.

---

- Manifest
- Using Secrets with Pods

{{% /section %}}

---

{{% section %}}

# Sidecar model

---

## Objective

By the end of the session you will be able to run multiple containers in a pod.

---

- What Does a Sidecar Container Do?
- Accessing Containers

{{% /section %}}

---

{{% section %}}

# Logging

---

## Objective

By the end of the session you will know where to look for information to troubleshoot a container application problem.

---

- Logging
- Basic
- Node Level
- Cluster Level

{{% /section %}}

---

{{% section %}}

# Managing container resources

---

## Objective

By the end of the session you will be able to limit resources to container applications

---

- Requests and Limits
- Resource Types
- Meaning of CPU
- Meaning of Memory

{{% /section %}}

---

{{% section %}}

# Network policies

---

## Objective

By the end of the session you will be able to manage how container applications communicate.

---

- Isolated and Non-isolated pods
- The NetworkPolicy Resource
- Selectors

{{% /section %}}

---

{{% section %}}

# Security contexts

---

## Objective

By the end of the session you will be able to explain Kubernetes security contexts

---

- Security Context
- Working with a Pod

{{% /section %}}

---

![Its over](/itsover.png "It's over")