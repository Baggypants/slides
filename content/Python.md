+++

title = "Python Slides"

outputs = ["Reveal"]

layout = "list"
[reveal_hugo]
theme = "white"

[logo]
src = "../qa.png"
width = "46px"


+++

# Programming with Python

Instructor: Leon L. Robinson

<https://linkedin.com/in/leonlrobinson>

- RHCA, RHCE
- Linux+, Network+, Security+
- TAP Diploma in Learning Facilitation

---

## Environment

---

## Objective 

By the end of the session you wil be able to create a comfortable environment for programming

---

- Python
- VSCode
- Github

---


## Introduction to Python

---

## Objective 

By the end of the session you will be able to explain the benefits of Python and perform some basic programming.

---

- What is Python
- Benefits
- Programming Concepts
- Variables
- Types
- IDLE
- Syntax
- Mathematic operators

---

## Data Types

---

## Objective

By the end of the session you will be able to manipulate data between types and format output.

---

- Data Types
- Managing String 
- Managing Numbers
- Managing Booleans

---

## Collections

---

## Objective

By the end of the session you will be able to manage collections of data

---

- Lists
- Tuples
- Dictionaries
- Sets

---

## Conditionals

---

# Objective

By the end of the session you will be able to use selection to determine a programs execution

---

- Conditionals
- Syntax
- Comparators

---

## Iteration

---

## Objective

By the end of the session you will be able to loop code to improve program efficiency

---

- Overview
- For
- While
- Loop Functionaility

---

## Functions

---

## Objective

By the end of the session you will be able to use functions

---

- Procedures
- Functions

---

## Modules

---

## Objective

By the end of the session you will be able to manage your code using modules

---

- Creating Modules
- Standard Modules
- Pip packages
- Virtual Environments

---

## Debugging

---

By the end of the session you will be able to explain types of debugging and use a debugging program

---

- What is debugging
- Static Analysis
- Dynamic Analysis
- Tools

---
## Classes

---

## Objective

By the end of the session you will be abel to create your own objects

---

- Understanding Classes
- Class Constructors
- Private constructs

---

## Object Oriented Programming

---

## Objective

By the end of the session you will be able to explain the 4 principles of Object Oriented Programming

---

- Inheritance
- Polymorphism
- Encapsulation
- Abstraction

---

## Files

---

## Objective

By the end od the session you will be able to move data in and out of files.

---

- Types of File
- Opening and Closing
- Reading
- Writing
- Best Practice

---

## Unit testing

---

## Objective

By the end of the session you will be able to write an automated test

---

- Theory of Testing
- Types of Testing
- Unit Testing in Python

---
