+++

title = "Python Slides"

outputs = ["Reveal"]

layout = "list"
[reveal_hugo]
theme = "white"

[logo]
src = "../qa.png"
width = "46px"


+++

# ATOS

Instructor: Leon L. Robinson

<https://linkedin.com/in/leonlrobinson>

- RHCA, RHCE
- Linux+, Network+, Security+
- TAP Diploma in Learning Facilitation

---

# Linux

---

- Introduction
- Distributions
- Bash
- Sessions
- Lists and Directories
- Editing

---

# CI CD

---

- Installation
- Jobs
- Freestyle Project
- Builds
- Plugins


