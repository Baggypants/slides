+++

title = "Slides"

outputs = ["Reveal"]

[reveal_hugo]
theme = "white"

[logo]
src = "qa.png"
width = "46px"


+++

# Instructor

Instructor: Leon L. Robinson

<https://linkedin.com/in/leonlrobinson>

- RHCA, RHCE
- Linux+, Network+, Security+
- TAP Diploma in Learning Facilitation
- Supremely good looking

---

## DevOps Basics

- What is DevOps?
- CI/CD
- Using the command line
- Agile
- Version Control

---

## What Is Devops?

---

### Objective

By the end of the session you will understand the culture of DevOps

---

- Overview
- How Things Used to be Done
- How DevOps Changes Things Up
  - Automation
    - Continuous Integration (CI)
    - Continuous Deployment/Delivery (CD)
    - Infrastructure as Code (IaC)
  - Measurement
    - Frequency of deployments
    - Mean time to recovery (MTTR)
    - Mean time to discovery (MTTD)
    - System availability
    - Service performance

---

## Using the Command Line

on Windows and Linux

---

### Objective

By the end of the session we will be able to use Command Line Interfaces.

---

- Overview
- Graphical User Interfaces vs Command Line Interfaces
- Commands
- Tutorial
  - Windows and Linux Command Prompts
    - Opening Command Prompt Terminal
    - Where Am I?
    - Making a Directory
    - Changing the Current Directory
    - Making a New File
    - Deleting Files and Directories
- Exercises

---

## CI/CD

---

### Objective

By the end of the session you will understand the tools and concepts surrounding Continuous Integration, Delivery and Deployment

---

- What CD does
- How CI works
- Benefits
- Challenges
- Exercise

---

- Continuous Delivery and Deployment
- Exercises

---

## Agile Fundamentals

---

### Objective

By the end of the session you will be able to explain Agile development values and principles.

---

- Software Development Lifecycle
- Waterfall
- Agile
  - Agile Manifesto
    - Agile Values
    - Agile Principles
    - Empirical Process Control
  - Common Agile development frameworks

---

## Version Control

---

## Objective

By the end of the session you will be able to use a source control management tool.

---

- Introduction
- Basics
- Cloning
- Forking
- Branching
- Merging
- Reverting

---

## Database Basics

- Learn the fundamentals of SQL
- Write SQL statements to create and manipulate a database
- Read SQL effectively
- Write quieries to join related tables

---

## Learn the Fundamentals of SQL

---

## Databases Introduction

---

- Installing
- Introduction
- Data Design
- Definition Language
- Entry Relationship Diagrams
- Manipulation
- Query Language
- Aggregates
- Joins

---

### Objective

By the end of the session you will.

---

## Git - SCM

---

- Introduction
- Basics
- Cloning
- Branching
- Merging
- Reverting
- Pull Requests

---

## Introduction

---

### Objective

By the end of the session you will have an understanding of source control management.

---

- Overview
- Source Control Management (SCM)
  - Repositories
  - Branching
  - Code Tracking
  - SCM Tools
  - Repository Hosting Services
- Tutorial
- Exercises

---

## Basics

---

### Objective

By the end of the session you will understand the basics of version control with git.

---

- Overview
- Basic Workflow
- Common Commands and Concepts
  - Cloning a Repository (git clone)
  - Staging a Change (git add)
  - Username and Email in Git Config (git config)
    - Setting Config Globally
    - Setting Config Locally
  - Local Repository Status (git status)
  - Commiting a Change (git commit)
  - Pushing Changes (git push)
  - Retrieving Remote Changes
  - Ignoring Files

  ---

## Cloning

---

### Objective

By the end of the session you will be able to clone a repository.

---

- Overview
  - Cloning a repository

---

## Forking

---

### Objective

By the end of the session you will be able to fork a repository.

---

- Overview
- Proposing a change
- Your idea
- Tutorial
  - Forking a repository
  - Updating forked repository from original

---

## Branching

---

### Objective

By the end of the session you will be able to create a branch.

---

- Overview
- Git Branching Workflow Example
  - New Application Features
  - Releases
  - Hotfixes
- Creating and Deleting Branches

---

## Merging

---

### Objective

By the end of the session you will be able to merge branches

---

## Reverting

---

### Objective

By the end of the session you will be able to revert changes

---

- Overview
- Reviewing the history of a repository
- Reverting to a previous commit with revert
- Reverting with reset
  - Using Revert to go Back to the Latest Commit

---

## Python

---

- introduction
- data-types
- collections
- conditionals
- iteration
- functions
- python-modul
- classes
- object-orien
- files
- debugging
- unit-testing

---

## Introduction to Python

---

### Objective

By the end of the session you will understand what python is and some benefits

---

## Errors

---

## Using Numbers

---

## Data Types

---

## Objective

By the end of the session you will know how to use data types in Python

---

## Collections

---

## Objective

By the end of the session you will be able to store and retrive data from collections

---

## Conditionals

---

### Objective

By the end of the session you will be able to branch code using conditionals

---

## Iteration

---

### Objective

By the end of the session you will be able to iterate over two types of loop

---

## Procedures and Functions

---

### Objective

By the end of the session you will be able to re-use code with Procedures and Functions

---

## Modules

---

### Objective

By the end of the session you will be able to extend functionality with modules

---

## Classes and Attributes

---

### Objective

By the end of the session you will understand the building blocks of Object Oriented Programming

---

## Object Oriented Programming

---

### Objective

By the end of the session you will be able to explain the 4 principles of Object Oriented Programming

---

## Files

---

### Objective

By the end of the session you will be able to move data in and out of files

---

## Debugging

---

### Objective

By the end of the session you will be able to step through programs and show variables with the pdb module

---

### Unit Test

---

### Objective

By the end of the session you will be able to write unit tests for your applications functions

---

## HTML and CSS

---

- Html and structure
- Tags and formatting
- special elements

---

## Html and Structure

---

### Objective

By the end of the session you will understand html page structure and the Document Object Model.

---

## Tags and formatting

---

### Objective

By the end of the session you will be able to organise and format text using html

---

## Special Elements

---

### Objective

By the end of the session you will be able to use special html elements to display images, lists, links and tables

---

## CSS

---

### Objective

By the end of the session you will be able to perform simple design using stylesheets.

---

## Javascript

---

- Running Javascript
- Events
- Functions
- Variables
- Conditional Logic
- Forms

---

## Running Javascript

---

### Objective

By the end of the session you will be able to use javascript to display information in a browser

---

## Events

---

### Objective

By the end of the session you will be able to execute javascript in response to events

---

## Functions

---

### Objective

By the end of the session you should be able to organise your code into functions

---

## Variables

---

### Objective

By the end of the session you will be able to set and manage variables in javascript programs

---

### Conditional Logic

---

### Objective

By the end of the session you will be able to use conditional statements in javascript

---

## Forms

---
