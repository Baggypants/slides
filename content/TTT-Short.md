
+++

title = "Slides"

outputs = ["Reveal"]

layout = "list"

[reveal_hugo]
theme = "white"

[logo]
src = "../qa.png"
width = "46px"

+++

# Train the Trainer

---

- Structured Learning
  - How Students Learn
    - Malcolm Knowles' Five Principles of Andragogy
    - Taking Effective Notes
  - Objectives and tests
    - The three essential elements of a well-structured objective
    - Assessing that knowledge and/or skills transfer has taken place
    - Assessing the achievement of the learning event objectives

---

- Session Structure
  - Objectives
  - Route Map
  - Capturing experience
  - Delivering
    - Question based
    - Story driven
  - Trainer activity
  - Praise
  - Signposting
  - Questioning techniques
  - Taking questions.
  - Reflective questioning

---

- Challenging Behaviour
  - Functions of challenging behaviour
  - Interacting with learner
  - Dealing with disclosure

  ---

# Structured Learning

---

### Objective

By the end of the session you will be able to understand how adults learn and confirm learning has taken place

---

# Session Structure

---

### Objective

By the end of the session you will be able to structre a learning session

---

# Challenging Behaviour

---

### Objective

By the end of the session you will be able to better manage challenging behaviour