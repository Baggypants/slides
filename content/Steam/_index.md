+++

title = "Slides"

outputs = ["Reveal"]

[reveal_hugo]
theme = "white"
custom_css = "custom.css"

[logo]
src = "qa.png"
width = "46px"


+++

# Instructor

Instructor: Leon L. Robinson

<https://linkedin.com/in/leonlrobinson>

- RHCA, RHCE
- Linux+, Network+, Security+
- TAP Diploma in Learning Facilitation
- Supremely good looking

---
{{< slide background-color="#004050" >}}

# Steam Locomotive Design

---

### Objective

By the end of the session you will understand the design of a Steam Locomotive

---

Steam Locomotive

![White](/white7.jpg)





---

![Its over](/Rocket.jpg)

---
