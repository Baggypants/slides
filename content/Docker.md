+++

title = "Docker Slides"

outputs = ["Reveal"]

layout = "list"
[reveal_hugo]
theme = "white"

[logo]
src = "../qa.png"
width = "46px"


+++

# Docker

Instructor: Leon L. Robinson

<https://linkedin.com/in/leonlrobinson>

- RHCA, RHCE
- Linux+, Network+, Security+
- TAP Diploma in Learning Facilitation

---

## Introduction

---

## Objective

By the end of the session you will be able to run a containerized application

---

## Images

---

## Objective

By the end of the session you will be able to pull and tag images

---

- Image layers
        - Registry
        - Image properties
- Docker image commands

---


## Containers

---

## Objective 

By the end of the session you will be able to manage a containers lifecycle.

---

- Use Case
- Containers
- Starting up a container
- Container information
- Executing commands in container
- Start, stop, remove existing containers
- Rename

---

## Dockerfiles

---

## Objective

By the end of the session you will be able to build docker images

---

- Dockerfiles

---

## Dockerfile Instructions

---

## Objective

By the end of the session you will be able to automate building docker images

---

- Instructions

---

## Multi-stage build

---

# Objective

By the end of the session you'll be able to automate a multi-stage build container 

---

- Source Code and Build Tools
- Multi-stage Builds
- Dockerfile Syntax

---

## Bind Mounts

---

## Objective

By the end of the session you'll be able to add persistent data to a container

---

- Bind mounts
        - Use case
        - Command


---

## Volumes
---

## Objective

By the end of the session you'll be able to add persistent data to a container using volumes

---

- Volume Benefits
- Managing Volumes
- Mounting Volumes
- Volume Drivers

---

## Networking
---

## Objective

By the end of the session you will understand and manage networking for containers

---

- Networking
- Network Types
- Bridge Networks
- Managing networks

---

## Registry

---

## Objective

By the end of the session you will be able to manage images in registries

---

- Storing and Pulling Images
- Creating Your Own Registry
- Accessing a Registry Remotely

---
